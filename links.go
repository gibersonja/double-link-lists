package main

import("fmt")

    type linkList struct {
        data int
        next *linkList
        source *linkList
    }


func main() {
    var testData linkList
    testData.data = 0
    testData.next = recurse(testData.data, testData)
    printRec(testData)
}

func printRec(link linkList) {

    if link.next != nil {
        fmt.Printf("DATA: %d\n",link.data)
        fmt.Printf("NEXT: %v\n",link.next)
        fmt.Printf("SRC:  %v\n",link.source)
        nextLink := *link.next
        printRec(nextLink)
    }
}

func recurse(x int, list linkList) *linkList {
    x = x + 1
    var newData linkList
    if x < 100 {
        newData.data = x
        newData.source = &list
        newData.next = recurse(x,newData)
    }
    return &newData
}
